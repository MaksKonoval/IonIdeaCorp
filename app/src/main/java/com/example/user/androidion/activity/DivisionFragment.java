package com.example.user.androidion.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.androidion.R;

public class DivisionFragment extends Fragment {
    public DivisionFragment() {
        // Требуемый пустой публичный конструктор
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_division_list, container, false);

        return rootView;
    }
}
