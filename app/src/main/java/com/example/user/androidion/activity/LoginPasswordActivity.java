package com.example.user.androidion.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.androidion.App;
import com.example.user.androidion.R;
import com.example.user.androidion.api.Api;
import com.example.user.androidion.api.provider.LoginProvider;
import com.example.user.androidion.api.result.LoginResult;
import com.example.user.androidion.data.User;

import java.io.IOException;

import io.reactivex.Observable;
import io.realm.Realm;

public class LoginPasswordActivity extends AppCompatActivity {
    private final String TAG = LoginPasswordActivity.class.getSimpleName();

    private Realm realm;
    private Api api;

    private EditText editLogin;
    private EditText editPassword;
    private Button login;

    private String androidId;

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Детализация логов
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_login);
        realm = Realm.getDefaultInstance();

        api = App.getApi();

        editLogin = findViewById(R.id.editLogin);
        editPassword = findViewById(R.id.editPassword);
        login = findViewById(R.id.button_login);

        androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public void Login(View view) throws IOException {
        // Блокируем кнопку логина
        login.setEnabled(false);

        // Проверям заполенность полей
        if (editLogin.getText().toString().equals("") || editPassword.getText().toString().equals("")) {
            return;
        }

        LoginProvider loginProvider = new LoginProvider();

        Observable<User> userObservable = loginProvider.login(editLogin.getText().toString(), editPassword.getText().toString(), androidId);

        userObservable.subscribe(user -> {
            Realm.getDefaultInstance().executeTransaction(realm -> {
                realm.copyToRealmOrUpdate(user);
            });
            Toast.makeText(getApplicationContext(), "Вход выполнен! " + user.getUsername(), Toast.LENGTH_SHORT).show();
            // Разблокируем кнопку логина
                    login.setEnabled(true);
                    Intent intent = new Intent(LoginPasswordActivity.this, MainActivity.class);
                    startActivity(intent);
        }, throwable -> {
            Toast.makeText(getApplicationContext(), "Неправильные данные! ", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Login: " + throwable);
            login.setEnabled(true);
        });

        // Делаем запрос на сервер

//        api.login(editLogin.getText().toString().trim(), editPassword.getText().toString().trim(), androidId)
//                .subscribe((LoginResult loginResult) -> { // Ответ положительный
//                    // Получем юзера из серверного ответа
//                    User user = loginResult.getUser();
//
//                    // Сохрнаняем юзера в базу
//                    realm.beginTransaction();
//                    realm.copyToRealmOrUpdate(user);
//                    realm.commitTransaction();
//
//                    Toast.makeText(getApplicationContext(), "Вход выполнен! " + user.getUsername(), Toast.LENGTH_SHORT).show();
//
//                    // Разблокируем кнопку логина
//                    login.setEnabled(true);
//                    Intent intent = new Intent(LoginPasswordActivity.this, MainActivity.class);
//                    startActivity(intent);
//                }, (Throwable e) -> { // Ответ отрицательный
//                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(), "Неправильные данные! ", Toast.LENGTH_SHORT).show();
//                    // Разблокируем кнопку логина
//                    login.setEnabled(true);
//                });
    }
}
