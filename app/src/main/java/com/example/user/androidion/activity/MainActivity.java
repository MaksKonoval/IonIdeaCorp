package com.example.user.androidion.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.example.user.androidion.App;
import com.example.user.androidion.R;
import com.example.user.androidion.adapter.ActivityListAdapter;
import com.example.user.androidion.api.Api;
import com.example.user.androidion.api.provider.DivisionsProvider;
import com.example.user.androidion.api.provider.EmployeesProvider;
import com.example.user.androidion.data.Division;
import com.example.user.androidion.data.Employee;

import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity {
    private ViewPager viewPager;
    public String title;

    private TabLayout tabLayout;
    private String[] pageTitle = {"Employees", "Divisions"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        viewPager = (ViewPager)findViewById(R.id.view_pager);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        //set gravity for tab bar
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set viewpager adapter
        ActivityListAdapter pagerAdapter = new ActivityListAdapter(getSupportFragmentManager(), this);

        viewPager.setAdapter(pagerAdapter);

        tabLayout.setupWithViewPager(viewPager);

//        FragmentManager fm  = getSupportFragmentManager();
//        switch(item.getItemId()) {
//            case R.id.:
//                title="Sample App";
//                viewPager.setCurrentItem(0);
//                break;
//
//            case R.id.nav_gallery:
//                viewPager.setCurrentItem(1);
//                break;
//


//            case R.id.nav_manage:
//                Intent intent = new Intent(MainActivity.this,NextActivity.class);
//                startActivity(intent);
//                break;

        }













































//// Найти пейджер представления, который позволит пользователю
//        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
//        // Создаем адаптер, который знает, какой фрагмент должен отображаться на каждой странице.
//        ActivityListAdapter adapter = new ActivityListAdapter(this, getSupportFragmentManager());
//// Установите адаптер на просмотр пейджера
//        viewPager.setAdapter(adapter);
//// Даем TabLayout ViewPager
//        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
//        tabLayout.setupWithViewPager(viewPager);
//
////        fragments.add(FRAGMENT_ONE, new DivisionFragment());
////        fragments.add(FRAGMENT_TWO, new EmployeeFragment());
////        // Настройка фрагментов, определяющих количество фрагментов, экраны и название.
////        fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
////
////            @Override
////            public int getCount() {
//
//                return FRAGMENTS;
//            }
//
//            @Override
//            public Fragment getItem(final int position) {
//
//                return fragments.get(position);
//            }
//
//            @Override
//            public CharSequence getPageTitle(final int position) {
//
//                switch (position) {
//                    case FRAGMENT_ONE:
//                        return "Title One";
//                    case FRAGMENT_TWO:
//                        return "Title Two";
//                    default:
//                        return null;
//                }
//            }
//        };
//        viewPager = (ViewPager) findViewById(R.id.pager);
//        viewPager.setAdapter(fragmentPagerAdapter);
//        viewPager.setCurrentItem(1);
//    }


//        Button getDivision = findViewById(R.id.get_division);
//        Button getEmployees = findViewById(R.id.get_employees);

        Realm realm = Realm.getDefaultInstance();
        Api api = App.getApi();


        // Получаем список дивизионов с сервера

    public void getEmployees(View view) {
        EmployeesProvider employeesProvider = new EmployeesProvider();

        Observable<List<Employee>> employeesResultObservable = employeesProvider.getEmployees();
        employeesResultObservable.subscribe(employees -> {
            Realm.getDefaultInstance().executeTransaction(realm ->
                    realm.copyToRealmOrUpdate(employees));
        });
//
    }

    public void getDivisions(View view) {
        DivisionsProvider divisionsProvider = new DivisionsProvider();
        Observable<List<Division>> divisionsResultObservable = divisionsProvider.getDivisions();
        divisionsResultObservable.subscribe(divisions -> {
            Realm.getDefaultInstance().executeTransaction(realm ->
                    realm.copyToRealmOrUpdate(divisions));
        });
    }

//        api.getDivisions()
//                .subscribe((DivisionsResult divisionsResult) -> { // Ответ положительный
//                    List<Division> divisions = divisionsResult.getDivisions();
//                    // Записуем дивизионы в базу
//                    realm.beginTransaction();
//                    realm.copyToRealmOrUpdate(divisions);
//                    realm.commitTransaction();
//                }, (Throwable e) -> { // Ответ отрицательный
//                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(), "Get Divisions error! ", Toast.LENGTH_SHORT).show();
//                });

//        // Получаем список сотрудников с сервера
//        api.getEmployees(true, true)
//                .subscribe((EmployeesResult employeesResult) -> { // Ответ положительный
//                    List<Employee> employees = employeesResult.getEmployees();
//                    // Записуем сотрудников в базу
//                    realm.beginTransaction();
//                    realm.copyToRealmOrUpdate(employees);
//                    realm.commitTransaction();
//                }, (Throwable e) -> { // Ответ отрицательный
//                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(), "Get Employees error! ", Toast.LENGTH_SHORT).show();
//                });

//        getDivision.setOnClickListener(viewDivision -> this.selectFragment(DivisionFragment.class));
//        getEmployees.setOnClickListener(viewEmployees -> this.selectFragment(EmployeeFragment.class));


//    public void pageFragment(Class fragmentClass) {
//        // Переключаем фрагменты
//        try {
//            Fragment fragment = (Fragment) fragmentClass.newInstance();
//            this.fragmentManager = getSupportFragmentManager();
//            this.fragmentManager
//                    .beginTransaction()
//                    .replace(R.id.container, fragment)
//                    .commit();
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
//    }

//    @Override
//    public void onEmployeeClick(Employee employee) {
//        // Нажатие на блок с инфой о сотруднике
//        Toast.makeText(getApplicationContext(), employee.toString(), Toast.LENGTH_SHORT).show();
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}