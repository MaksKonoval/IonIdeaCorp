package com.example.user.androidion.activity.employee;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.androidion.R;
import com.example.user.androidion.activity.RecyclerViewModel;
import com.example.user.androidion.data.Employee;

import java.util.ArrayList;

public class EmployeeFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,@Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_employee_list, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final ArrayList<RecyclerViewModel> pname = new ArrayList<RecyclerViewModel>();
////
////        pname.add(new RecyclerViewModel("Alpha"));
////        pname.add(new RecyclerViewModel("Beta"));
////        pname.add(new RecyclerViewModel("Cupcake"));
////        pname.add(new RecyclerViewModel("Donut"));
////        pname.add(new RecyclerViewModel("Eclairs"));
////        pname.add(new RecyclerViewModel("Froyo"));
////        pname.add(new RecyclerViewModel("GingerBread"));
////        pname.add(new RecyclerViewModel("HoneyComb"));
////        pname.add(new RecyclerViewModel("IceCreamSandwich"));
////        pname.add(new RecyclerViewModel("JellyBean"));
////        pname.add(new RecyclerViewModel("KitKat"));
////        pname.add(new RecyclerViewModel("Lollipop"));
////        pname.add(new RecyclerViewModel("MarshMallow"));
////        pname.add(new RecyclerViewModel("Nougat"));
//
        final RecyclerViewAdapter itemsAdapter = new RecyclerViewAdapter(EmployeeFragment.this.getActivity(), pname, null);
        final RecyclerView clv = (RecyclerView) view.findViewById(R.id.clist);
        clv.setLayoutManager(new LinearLayoutManager(EmployeeFragment.this.getActivity()));
        clv.setHasFixedSize(true);
        clv.setAdapter(itemsAdapter);

    }

}

//        // Set the adapter
//        if (view instanceof RecyclerView) {
//            Context context = view.getContext();
//            RecyclerView recyclerView = (RecyclerView) view;
//            recyclerView.setLayoutManager(new LinearLayoutManager(context));
//
//            EmployeeRepo repo = new RealmEmployeeRepo();
//            List<Employee> employees = repo.findAll(false);
//
//            recyclerView.setAdapter(new RecyclerViewAdapter(employees, mListener));
//        }
//
//        return view;
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof IOnEmployeeClick) {
//            mListener = (IOnEmployeeClick) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement IOnEmployeeClick");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }
//
//    public interface EmployeeClick {
//        void onEmployeeClick(Employee item);
//    }
//}
