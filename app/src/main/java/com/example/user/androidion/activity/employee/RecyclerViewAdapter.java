package com.example.user.androidion.activity.employee;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.androidion.R;
import com.example.user.androidion.activity.RecyclerViewModel;
import com.example.user.androidion.data.Employee;

import java.util.ArrayList;
import java.util.List;



public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private static final String LOG_TAG = RecyclerViewAdapter.class.getSimpleName();
    private ArrayList<RecyclerViewModel> mItems;
    ItemListener mListener;
    Context context;
    public static int position;

    public RecyclerViewAdapter(Activity context, ArrayList<RecyclerViewModel> program, ItemListener listener) {

        this.context = context;
        mItems = program;
        mListener = listener;
    }

    public void setOnItemClickListener(ItemListener listener) {
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item, parent, false);
        context = parent.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CardView cardView;
        public RecyclerViewModel pName;
        TextView name;
        View textContainer;

        public ViewHolder(View itemView) {

            super(itemView);
            itemView.setOnClickListener(this);
            cardView = (CardView)itemView.findViewById(R.id.cvItem);
            name = (TextView) itemView.findViewById(R.id.list_text);
            textContainer = itemView.findViewById(R.id.text_container);
        }

        public void setData(RecyclerViewModel pName) {
            this.pName = pName;
            name.setText(pName.getmItemName());
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(pName, getAdapterPosition());
            }
            Toast.makeText(context,pName.getmItemName(),Toast.LENGTH_SHORT).show();

        }

    }

    public interface ItemListener {
        void onItemClick(RecyclerViewModel pName, int position);
    }
}


//    private final List<Employee> mValues;
//
//    public RecyclerViewAdapter(List<Employee> items) {
//        mValues = items;
//    }
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.fragment_employee, parent, false);
//        return new ViewHolder(view);
//    }

//    @SuppressLint("SetTextI18n")
//    @Override
//    public void onBindViewHolder(final ViewHolder holder, int position) {
//        holder.mItem = mValues.get(position);
//        holder.mIdView.setText(mValues.get(position).getId().toString());
//        holder.mFirstNameView.setText(mValues.get(position).getFirstName());
//        holder.mLastNameView.setText(mValues.get(position).getLastName());
//        holder.mSalaryView.setText(mValues.get(position).getSalary());
//        holder.mDivisionNameView.setText(mValues.get(position).getDivisionName());
//
//        holder.mView.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                if (null != mListener) {
////                    // Notify the active callbacks interface (the activity, if the
////                    // fragment is attached to one) that an item has been selected.
////                    mListener.onEmployeeClick(holder.mItem);
////                }
////            }
////        });
//    }

//    @Override
//    public int getItemCount() {
//        return mValues.size();
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//        public final View mView;
//        public final TextView mIdView;
//        public final TextView mFirstNameView;
//        public final TextView mLastNameView;
//        public final TextView mSalaryView;
//        public final TextView mDivisionNameView;
//        public Employee mItem;
//
//        public ViewHolder(View view) {
//            super(view);
//
//            mView = view;
//            mIdView = view.findViewById(R.id.id);
//            mFirstNameView = view.findViewById(R.id.first_name);
//            mLastNameView = view.findViewById(R.id.last_name);
//            mSalaryView = view.findViewById(R.id.salary);
//            mDivisionNameView = view.findViewById(R.id.division_name);
//        }
//
//        @Override
//        public String toString() {
//            return super.toString() + " '" + mIdView.getText() + "'";
//        }
//    }

