package com.example.user.androidion.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.user.androidion.R;
import com.example.user.androidion.activity.DivisionFragment;
import com.example.user.androidion.activity.employee.EmployeeFragment;


public class ActivityListAdapter extends FragmentPagerAdapter {

    private Context mContext;


    public ActivityListAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new EmployeeFragment();
        } else {
            return new DivisionFragment();


        }
    }

    // Определяет количество вкладок
    @Override
    public int getCount() {

        return 2;
    }

//    // Это определяет заголовок для каждой вкладки
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return mContext.getString(R.string.employees);
            case 1:
                return mContext.getString(R.string.divisions);
            default:
                return null;
        }
    }
}
