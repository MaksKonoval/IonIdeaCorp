package com.example.user.androidion.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.user.androidion.R;
import com.example.user.androidion.data.Division;
import com.example.user.androidion.data.Employee;

import java.util.List;


public class DivisionAdapter extends RecyclerView.Adapter<DivisionAdapter.ViewHolder> {

    private List<Division> divisions;
    private Context context;

    public DivisionAdapter(Context context) {
        this.context = context;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, name;

        public ViewHolder(View listItem) {
            super(listItem);
            id = listItem.findViewById(R.id.id);
            name = listItem.findViewById(R.id.name);
        }

    }

    public void setDivisions(List<Division> divisions) {
        this.divisions = divisions;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.item, parent, false);
        ViewHolder vh = new ViewHolder(rowView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int index = holder.getAdapterPosition();
        holder.id.setText(divisions.get(index).getId());
        holder.name.setText(divisions.get(index).getName());
    }

    @Override
    public int getItemCount() {
        return divisions == null ? 0 : divisions.size();
    }
}
