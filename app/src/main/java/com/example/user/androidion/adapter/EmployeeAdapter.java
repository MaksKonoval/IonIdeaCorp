package com.example.user.androidion.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.user.androidion.R;
import com.example.user.androidion.data.Employee;

import java.util.List;


public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.ViewHolder> {

    private List<Employee> employees;
    private Context context;

    public EmployeeAdapter(Context context) {
        this.context = context;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView firstName, lastName;

        public ViewHolder(View listItem) {
            super(listItem);
            firstName = listItem.findViewById(R.id.first_name);
            lastName = listItem.findViewById(R.id.last_name);
        }

    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.item, parent, false);
        ViewHolder vh = new ViewHolder(rowView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int index = holder.getAdapterPosition();
        holder.firstName.setText(employees.get(index).getFirstName());
        holder.lastName.setText(employees.get(index).getLastName());
    }

    @Override
    public int getItemCount() {
        return employees == null ? 0 : employees.size();
    }
}
