package com.example.user.androidion.api;

import com.example.user.androidion.api.result.DivisionsResult;
import com.example.user.androidion.api.result.EmployeesResult;
import com.example.user.androidion.api.result.LoginResult;
import com.example.user.androidion.data.User;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface Api {
    @POST("/login")
//         @GET("/bins/pqrn5")
    Call<LoginResult> login(
            @Query("username") String username,
            @Query("password") String password,
            @Query("device_id") String deviceId
    );

    @GET("/division")
//         @GET("/bins/1hetqt")
    Call<DivisionsResult> getDivisions();

    @GET("/employee")
//    @GET("/bins/107ed1")
    Call<EmployeesResult> getEmployees(
            @Query("active") Boolean active,
            @Query("inactive") Boolean inactive
    );

    @POST("/employee")
    Call<EmployeesResult> addEmployee(
            @Header("Auth-token") String token,
            @Query("first_name") Integer firstName,
            @Query("last_name") Integer lastName,
            @Query("salary") String salary,
            @Query("birthday") String birthday,
            @Query("active") Boolean active,
            @Query("division_id") Integer divisionId
    );

    @PUT("/employee")
//     @PUT("/bins/1cpi2d")
    Call<EmployeesResult> updateEmployee(
            @Query("id") Integer id,
            @Query("first_name") Integer firstName,
            @Query("last_name") Integer lastName,
            @Query("salary") String salary,
            @Query("birthday") String birthday,
            @Query("active") Boolean active,
            @Query("division_id") Integer divisionId
    );
}
