package com.example.user.androidion.api.provider;

import com.example.user.androidion.App;
import com.example.user.androidion.api.result.DivisionsResult;
import com.example.user.androidion.data.Division;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DivisionsProvider {
//    Realm realm = Realm.getDefaultInstance();

    public Observable<List<Division>> getDivisions() {
        return Observable.create(emitter -> {
            App.getApi().getDivisions().enqueue(new Callback<DivisionsResult>() {
                @Override
                public void onResponse(Call<DivisionsResult> call, Response<DivisionsResult> response) {
//                    if (response.isSuccessful()) {
                    emitter.onNext(response.body().getDivisions());

//                        List<Division> divisions = response.body().getDivisions();
//                        for (int i = 0; i < divisions.size(); i++) {
//                            divisionsRealm.add(realm.copyToRealm(divisions.get(i)));
//                        }
//                        realm.commitTransaction();
//                    }
//                    emitter.onNext(divisionsRealm);
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<DivisionsResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });

        });
    }}





