package com.example.user.androidion.api.provider;

import com.example.user.androidion.App;
import com.example.user.androidion.api.result.EmployeesResult;
import com.example.user.androidion.data.Employee;

import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class EmployeesProvider {
//    Realm realm = Realm.getDefaultInstance();

    public Observable<List<Employee>> getEmployees() {
        return Observable.create(emitter -> {
//            final RealmResults<Employee> employees = realm.where(Employee.class).findAll();
//            emitter.onNext(employees);
//            emitter.onComplete();
//
            App.getApi().getEmployees(false, false).enqueue(new Callback<EmployeesResult>() {
                @Override
                public void onResponse(Call<EmployeesResult> call, Response<EmployeesResult> response) {
//                    if (response.isSuccessful()) {
                        emitter.onNext(response.body().getEmployees());
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<EmployeesResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });
        });
    }
}
