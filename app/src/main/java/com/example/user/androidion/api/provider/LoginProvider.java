package com.example.user.androidion.api.provider;

import android.util.Log;

import com.example.user.androidion.App;
import com.example.user.androidion.api.result.LoginResult;
import com.example.user.androidion.data.User;

import java.io.IOException;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class LoginProvider {

    private final String TAG = LoginProvider.class.getSimpleName();

    public Observable<User> login(String username, String password, String deviceId) {
        return Observable.create(emitter -> {
            App.getApi().login(username, password, deviceId).enqueue(new Callback<LoginResult>() {
                @Override
                public void onResponse(Call<LoginResult> call, Response<LoginResult> response) {
                    if (response.isSuccessful()) {
                        emitter.onNext(response.body().getUser());
                    } else {
                        try {
                            Log.e(TAG, "login response: " + response.errorBody().string());
                        } catch (IOException e) {
                            Log.e(TAG, "login response: " + e);
                        }
                    }
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<LoginResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });
        });
    }
}
