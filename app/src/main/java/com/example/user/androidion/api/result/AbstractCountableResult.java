package com.example.user.androidion.api.result;

import com.google.gson.annotations.SerializedName;

abstract public class AbstractCountableResult {
    @SerializedName("count")
    protected Integer count;

//    public AbstractCountableResult(Integer count) {
//        this.count = count;
//    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
