package com.example.user.androidion.api.result;

import com.example.user.androidion.data.Division;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DivisionsResult extends AbstractCountableResult {
    @SerializedName("division")
    private List<Division> divisions;

//    public DivisionsResult(Integer count, List<Division> divisions) {
//        super(count);
//        this.divisions = divisions;
//    }

    public List<Division> getDivisions() {
        return divisions;
    }

    public void setDivisions(List<Division> divisions) {
        this.divisions = divisions;
    }
}
