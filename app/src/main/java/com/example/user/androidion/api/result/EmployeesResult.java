package com.example.user.androidion.api.result;

import com.example.user.androidion.data.Employee;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmployeesResult extends AbstractCountableResult {
    @SerializedName("employees")
    private List<Employee> employees;

//    public EmployeesResult(Integer count, List<Employee> employees) {
//        super(count);
//        this.employees = employees;
//    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
