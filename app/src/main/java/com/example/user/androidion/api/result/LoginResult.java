package com.example.user.androidion.api.result;

import com.example.user.androidion.data.User;
import com.google.gson.annotations.SerializedName;

public class LoginResult {
    @SerializedName("user")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
