package com.example.user.androidion.repo;

import android.support.annotation.Nullable;

import com.example.user.androidion.data.Employee;

import java.util.List;

import io.reactivex.Observable;
import io.realm.Sort;

public interface EmployeeRepo {
    Observable<Integer> getCurrentChangeObservable();

    List<Employee> findAll(boolean detached);

    List<Employee> findAllSorted(String sortField, Sort sort, boolean detached);

    Observable<List<Employee>> findAllSortedWithChanges(String sortField, Sort sort);

    @Nullable
    Employee getByField(String field, String value, boolean detached);

    void save(Employee country);

    void delete(Employee country);

    Employee detach(Employee country);
}
