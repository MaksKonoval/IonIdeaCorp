package com.example.user.androidion.repo;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;

import com.example.user.androidion.data.Employee;
import com.example.user.androidion.util.RealmResultsObservable;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

@SuppressLint("NewApi")
public class RealmEmployeeRepo implements EmployeeRepo {
    private Realm realm = Realm.getDefaultInstance();
    private final PublishSubject<Integer> currentChangeSubject = PublishSubject.create();

    public RealmEmployeeRepo() {
    }

    @Override
    public Observable<Integer> getCurrentChangeObservable() {
        return currentChangeSubject;
    }

    @Override
    public List<Employee> findAll(boolean detached) {
        RealmResults<Employee> realmResults = realm.where(Employee.class).findAll();

        if (detached) {
            return realm.copyFromRealm(realmResults);
        }

        return realmResults;
    }

    @Override
    public List<Employee> findAllSorted(String sortField, Sort sort, boolean detached) {
        RealmResults<Employee> realmResults = realm.where(Employee.class).findAllSorted(sortField, sort);

        if (detached) {
            return realm.copyFromRealm(realmResults);
        }

        return realmResults;
    }

    @Override
    public Observable<List<Employee>> findAllSortedWithChanges(String sortField, Sort sort) {
        return RealmResultsObservable.from(realm.where(Employee.class).findAllSortedAsync(sortField, sort))
                .filter(RealmResults::isLoaded)
                .map(result -> result);
    }

    @Override
    @Nullable
    public Employee getByField(String field, String value, boolean detached) {
        Employee realmEmployee = realm.where(Employee.class).equalTo(field, value).findFirst();
        if (detached && realmEmployee != null) {
            realmEmployee = realm.copyFromRealm(realmEmployee);
        }

        return realmEmployee;
    }

    @Override
    public void save(Employee employee) {
        realm.executeTransaction(r -> r.copyToRealmOrUpdate(employee));
        currentChangeSubject.onNext(employee.getId());
    }

    @Override
    public void delete(Employee realmEmployee) {
        if (realmEmployee.isValid()) {
            Integer id = realmEmployee.getId();
            realm.executeTransaction(r -> realmEmployee.deleteFromRealm());
            currentChangeSubject.onNext(id);
        }
    }

    @Override
    public Employee detach(Employee employee) {
        if (!employee.isManaged()) {
            return employee;
        }

        return realm.copyFromRealm(employee);
    }
}
