package com.example.user.androidion.repo;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;

import com.example.user.androidion.data.User;
import com.example.user.androidion.util.RealmResultsObservable;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

@SuppressLint("NewApi")
public class RealmUserRepo implements UserRepo {
    private final PublishSubject<Integer> currentChangeSubject = PublishSubject.create();

    private final Provider<Realm> realmProvider;

    @Inject
    public RealmUserRepo(Provider<Realm> realmProvider) {
        this.realmProvider = realmProvider;
    }

    @Override
    public Observable<Integer> getCurrentChangeObservable() {
        return currentChangeSubject;
    }

    @Override
    public List<User> findAllSorted(String sortField, Sort sort, boolean detached) {
        try (Realm realm = realmProvider.get()) {
            RealmResults<User> realmResults = realm.where(User.class).findAllSorted(sortField, sort);

            if (detached) {
                return realm.copyFromRealm(realmResults);
            } else {
                return realmResults;
            }
        }
    }

    @Override
    public Observable<List<User>> findAllSortedWithChanges(String sortField, Sort sort) {
        try (Realm realm = realmProvider.get()) {
            return RealmResultsObservable.from(realm.where(User.class).findAllSortedAsync(sortField, sort))
                    .filter(RealmResults::isLoaded)
                    .map(result -> result);
        }
    }

    @Override
    @Nullable
    public User getByField(String field, String value, boolean detached) {
        try (Realm realm = realmProvider.get()) {
            User realmUser = realm.where(User.class).equalTo(field, value).findFirst();
            if (detached && realmUser != null) {
                realmUser = realm.copyFromRealm(realmUser);
            }

            return realmUser;
        }
    }

    @Override
    public void save(User user) {
        try (Realm realm = realmProvider.get()) {
            realm.executeTransaction(r -> r.copyToRealmOrUpdate(user));
            currentChangeSubject.onNext(user.getId());
        }
    }

    @Override
    public void delete(User realmUser) {
        if (realmUser.isValid()) {
            try (Realm realm = realmProvider.get()) {
                Integer id = realmUser.getId();
                realm.executeTransaction(r -> realmUser.deleteFromRealm());
                currentChangeSubject.onNext(id);
            }
        }
    }

    @Override
    public User detach(User user) {
        if (!user.isManaged()) {
            return user;
        }

        try (Realm realm = realmProvider.get()) {
            return realm.copyFromRealm(user);
        }
    }
}
