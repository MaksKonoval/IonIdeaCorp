package com.example.user.androidion.repo;

import android.support.annotation.Nullable;

import com.example.user.androidion.data.User;

import java.util.List;

import io.reactivex.Observable;
import io.realm.Sort;

public interface UserRepo {
    Observable<Integer> getCurrentChangeObservable();

    List<User> findAllSorted(String sortField, Sort sort, boolean detached);

    Observable<List<User>> findAllSortedWithChanges(String sortField, Sort sort);

    @Nullable
    User getByField(String field, String value, boolean detached);

    void save(User country);

    void delete(User country);

    User detach(User country);
}
